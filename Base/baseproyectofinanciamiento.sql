create table Proyecto_financiamiento(
cve serial PRIMARY KEY,
nombre varchar NOT NULL,
cve_proy varchar NOT NULL,
cve_fondo varchar NOT NULL,
Fech_Ini date,
Fech_Fin date,
cve_Area int ,
FOREIGN KEY(cve_Area) REFERENCES areas(cve),
cve_ArC int 
);

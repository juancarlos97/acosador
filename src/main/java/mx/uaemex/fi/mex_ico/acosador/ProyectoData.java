package mx.uaemex.fi.mex_ico.acosador;

/**
 * Clase que simplemente transporta informaci&oacute;n
 * @author fchavez Francisco Ch&aacute;vez Casta&ntilde;eda.
 */
public class ProyectoData {
  /**
   * Descripci&oacute;n
   */
  private String descripcion;
  
  public String getDescripcion() {
    return this.descripcion;
  }
  public void setDescripcion(String text) {
    this.descripcion = text;
  }
  public String proyecto;

public String getProyecto() {
	return proyecto;
}
public void setProyecto(String proyecto) {
	this.proyecto = proyecto;
}
  
}

package mx.uaemex.fi.mex_ico.acosador.areas;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import mx.uaemex.fi.mex_ico.acosador.SecurityCheck;

@Component
public class AreasService {
	private static final Logger log = LoggerFactory.getLogger(AreasService.class);
	private AreasRepository repository;
	
	public AreasService(AreasRepository repository) {
		this.repository = repository;
	}
	
	@SecurityCheck
	@Transactional(noRollbackFor = { UnsupportedOperationException.class })
	public Areas save(String nombre) {
		Areas area = repository.guardarArea(new Areas(nombre));
		log.debug("Nuevo proyecto[id={}] salvado", area.getCve());
		return area;
	}
	
	@SecurityCheck
	@Transactional()
	public  void borrarArea(int cve) {
		 ArrayList<Areas> area = (ArrayList<Areas>) this.consultaArea(cve);
		 Areas ar = area.get(0);
		 repository.borraArea(ar);
		 log.debug("Area Borrada[id={}] adios", 1);
	 }
	
	@SecurityCheck
	@Transactional()
	public String mostrarareas(int clav) {
		ArrayList<Areas> area = (ArrayList<Areas>) this.consultaArea(clav);
		Areas ar = area.get(0);
		return ar.getNombre();
	}
	
	public List<Areas> consultaArea(int clave){
		return repository.consultaArea(clave);
	}
	
	@SecurityCheck
	@Transactional()
	public void modificaArea(int cve, String nombre){
		ArrayList<Areas> a = (ArrayList<Areas>) this.consultaArea(cve);
		Areas ar = a.get(0);
		ar.setNombre(nombre);
		repository.modificarArea(ar);
		log.debug("Area Modifcada", 1);
	}


}

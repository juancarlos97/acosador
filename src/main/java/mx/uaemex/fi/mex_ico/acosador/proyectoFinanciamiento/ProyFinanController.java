package mx.uaemex.fi.mex_ico.acosador.proyectoFinanciamiento;

import java.sql.Date;

import java.util.ArrayList;

import javax.xml.crypto.Data;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller //Con esto lo marcamos como controlador
@RequestMapping("/proyecto_financiamiento") //URI a la que responde este controlador
public class ProyFinanController {
  /**
   * Servicio para que el controlador tenga superpoderes.
   */
  private ProyFinanService proyectoFinService;
  /**
   * Constructora
   * @param proyService Servicio que ser&aacute; inyectado por Spring.
   * Conviene que lo inyecte Spring porque este a su vez necesitara
   * a los modelos ya configurados.
   */
  public ProyFinanController(ProyFinanService proyFinService) { //El servicio es inyectado por Spring
    this.proyectoFinService = proyFinService;
  }
 
	@DeleteMapping("borrar/{bor}")
	@ResponseBody
	public ResponseEntity<String> borrar(@PathVariable int bor){
		proyectoFinService.borrar(bor);
		return ResponseEntity.ok("borrado");
}	
	@PostMapping("editar/{nom}/{cvProy}/{cvFondo}/{fech_ini}/{fech_fin}/{cvA}/{cvAr}")
	@ResponseBody
	public ResponseEntity<String> editar(@PathVariable String nom, @PathVariable String cvProy,@PathVariable String cvFondo,@PathVariable Date fech_ini,@PathVariable Date fech_fin,@PathVariable int cvA,@PathVariable int cvAr){
		proyectoFinService.editar(nom, cvProy, cvFondo ,fech_ini, fech_fin, cvA, cvAr);
		return ResponseEntity.ok("Editado");
	}
	@PutMapping("actualizar/{cve}/{nom}/{cvProy}/{cvFondo}/{fech_ini}/{fech_fin}/{cvA}/{cvAr}")
	@ResponseBody
	public ResponseEntity<String> actualizar(@PathVariable int cve ,@PathVariable String nom, @PathVariable String cvProy,@PathVariable String cvFondo,@PathVariable Date fech_ini,@PathVariable Date fech_fin,@PathVariable int cvA,@PathVariable int cvAr){
		proyectoFinService.actualizar(cve, nom, cvProy, cvFondo, fech_ini, fech_fin, cvA,cvAr);
		return ResponseEntity.ok("Actualizado");
	}	
	
	@GetMapping("consultar")
	@ResponseBody
	public ResponseEntity<List<ProyectoFinaciamiento>> consultar(){
		List<ProyectoFinaciamiento> result = proyectoFinService.consultar();
		return ResponseEntity.ok(result);
	}
	
	@GetMapping("consultarPorClave/{cve}")
	@ResponseBody
	public ResponseEntity<ProyectoFinaciamiento> consultarPorClave(@PathVariable long cve){
		List<ProyectoFinaciamiento> result = proyectoFinService.verProy(cve);
		ProyectoFinaciamiento row = result.get(0);
		return ResponseEntity.ok(row);
	}
}
package mx.uaemex.fi.mex_ico.acosador.proyectoFinanciamiento;

import org.hibernate.query.Query;
import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProyFinanRepository {

  private SessionFactory sessionFactory;

  public ProyFinanRepository(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public ProyectoFinaciamiento saveProyecto(ProyectoFinaciamiento proyectoFin) {
	Session session = sessionFactory.getCurrentSession();
    session.save(proyectoFin);
    return proyectoFin;
  }
  
  public List<ProyectoFinaciamiento> consultaProyecto(long clave_service){
	  List<ProyectoFinaciamiento> proyectoFinaciamiento;
	  ProyectoFinaciamiento p= new ProyectoFinaciamiento();
	  Session session= sessionFactory.getCurrentSession();
	  
	  Query query = session.createQuery("FROM " + p.getClass().getName()+" WHERE cve ="+clave_service);
	  
	  List<ProyectoFinaciamiento> proyectoFina;
	  proyectoFina=query.list();
	  return proyectoFina;
	  }
  
  public List<ProyectoFinaciamiento> consultaProyectos(){
	  List<ProyectoFinaciamiento> proyectoFinaciamiento;
	  ProyectoFinaciamiento p= new ProyectoFinaciamiento();
	  Session session= sessionFactory.getCurrentSession();
	  
	  Query query = session.createQuery("FROM " + p.getClass().getName());
	  
	  List<ProyectoFinaciamiento> proyectoFina;
	  proyectoFina=query.list();
	  return proyectoFina;
  }
  
  public void deleteProyecto(ProyectoFinaciamiento reg) {
	  Session session= sessionFactory.getCurrentSession();
	  session.delete(reg);
  }
  public void insertProyecto(ProyectoFinaciamiento insert) {
	  Session session=sessionFactory.getCurrentSession();
	  session.save(insert);
  }
	  public void actualizarProyecto(ProyectoFinaciamiento clvb) {
		 Session session=sessionFactory.getCurrentSession();
		 session.update(clvb);
  }
}
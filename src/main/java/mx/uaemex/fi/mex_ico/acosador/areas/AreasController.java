package mx.uaemex.fi.mex_ico.acosador.areas;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/Areas")
public class AreasController {
	private AreasService areasService;
	
	public AreasController(AreasService areasService) {
		this.areasService = areasService;
	}
	
	@PostMapping("/agregarArea/{nombre}")
	@ResponseBody
	public ResponseEntity<String> agregarArea(@PathVariable String nombre){
		areasService.save(nombre);
		return ResponseEntity.ok("Area Agregada");
	}
	
	@DeleteMapping("/borrarArea/{cve}")
	@ResponseBody
	public ResponseEntity<String> borrarArea(@PathVariable int cve){
		areasService.borrarArea(cve);
		return ResponseEntity.ok("Area Borrada");
	}
	
	@GetMapping("/mostrar/{clave}")
	public String mostrar(@PathVariable int clave, Model model){ 
		String area =  areasService.mostrarareas(clave);
		model.addAttribute("mensaje","El area es: "+area);
		return "welcome";
	}

	@PutMapping("/modificarArea/{cve}/{nombre}")
	  @ResponseBody
	  
	  public ResponseEntity<String> modificar(@PathVariable int cve, @PathVariable String nombre){
		  if (nombre == null) {
			  return ResponseEntity.status(500).build();
			  
		  }else{
			  areasService.modificaArea(cve, nombre);
			  
			  return ResponseEntity.ok("Has modificado la area a: "+ nombre);
		  }
		 
		  
	  }
}

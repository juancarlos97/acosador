package mx.uaemex.fi.mex_ico.acosador;

import java.util.Arrays;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import mx.uaemex.fi.mex_ico.acosador.upload.storage.StorageProperties;

import javax.sql.DataSource;

@Configuration //Con esta anotacion lo hacemos un configurador
@ComponentScan("mx.uaemex.fi.mex_ico.acosador") //Donde buscar modelos
@EnableTransactionManagement //Habilita el manejo de transacciones 
public class AppConfig {
  /**
   * Fuente de datos, se configura mediante el application.properties
   * y se inyecta donde sea necesario.
   */
  private DataSource dataSource;
  private StorageProperties propiedades;
  /**
   * Constructora, aqui es inyectado el data source por Spring. 
   * @param dataSource Fuente de datos
   */
  public AppConfig(DataSource dataSource) { 
    this.dataSource = dataSource;
  }
  /**
   * M&eacute;todo para registrar un filtro en la aplicaci&oacute;n
   * este filtro es utilizado con la AOP para escribir a la bitacora
   * (log) cuando se invoquen metodos del controlador que responde
   * a la URI proyectos.
   * @return Un bean de registro de filtros
   */
  @Bean
  public FilterRegistrationBean<AuditingFilter> auditingFilterRegistrationBean() {
    FilterRegistrationBean<AuditingFilter> registration = new FilterRegistrationBean<>();
    AuditingFilter filter = new AuditingFilter(); //Creamos el filtro
    registration.setFilter(filter); //Se lo colocamos al bean de registro de filtros
    registration.setOrder(Integer.MAX_VALUE); //Le damos su posicion en la fila de filtros (en esta caso hasta el final)
    registration.setUrlPatterns(Arrays.asList("/proyectos/*")); //Ligamos al filtro a una URI
    return registration; //Regresamos el bean de registro de filtros
  }
  /**
   * Configuracion de la fabrica de sesiones.
   * @return Fabrica de sesiones
   */
  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
    sessionFactoryBean.setDataSource(dataSource); //Ponemos el data source en la fabrica de sesiones
    sessionFactoryBean.setPackagesToScan("mx.uaemex.fi.mex_ico.acosador"); //Le decimos donde buscar los modelos
    return sessionFactoryBean; //Retornamos la fabrica de sessiones
  }
  /**
   * Configuracion del administrador de transacciones
   * @return Un administrador de transacciones para hibernate
   */
  @Bean
  public HibernateTransactionManager transactionManager() {
    HibernateTransactionManager transactionManager = new HibernateTransactionManager();
    transactionManager.setSessionFactory(sessionFactory().getObject()); //Ligamos a la fabrica de sesiones con el manejador de transacciones
    return transactionManager; //Regresamos el administrador de tranasacciones
  }
}
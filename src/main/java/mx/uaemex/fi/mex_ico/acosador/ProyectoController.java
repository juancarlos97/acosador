package mx.uaemex.fi.mex_ico.acosador;

import java.util.ArrayList;
import java.util.Date;

import javax.xml.crypto.Data;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller //Con esto lo marcamos como controlador
@RequestMapping("/proyectos") //URI a la que responde este controlador
public class ProyectoController {
  /**
   * Servicio para que el controlador tenga superpoderes.
   */
  private ProyectoService proyectoService;
  /**
   * Constructora
   * @param proyService Servicio que ser&aacute; inyectado por Spring.
   * Conviene que lo inyecte Spring porque este a su vez necesitara
   * a los modelos ya configurados.
   */
  public ProyectoController(ProyectoService proyService) { //El servicio es inyectado por Spring
    this.proyectoService = proyService;
  }

  @GetMapping("/welcome") //Cuando llamen, mediante GET, a proyectos/welcome haces esto
  public String welcome(Model model) {
    model.addAttribute("mensaje", "Hola, Bienvenido a Spring Boot! perras, ya quedo");
    return "welcome";
  }
  
  @GetMapping("/adios") //Cuando llamen, mediante GET, a proyectos/adios hace esto
  public String adios(Model model) {
    model.addAttribute("mensaje", "Saliendo del mundo de Spring Boot!");
    return "adios";
  }
  @GetMapping("/elprro")
  public String elprro(Model model) {
	  model.addAttribute("mensaje", "Prro esto es de prueba");
	  return "elprro";
  }

  @PostMapping("/{}") //Cuando llamen a proyectos mediante un POST haces esto
  @ResponseBody
  public ResponseEntity<Proyecto> saveProyecto(@RequestBody ProyectoData data) {
    //El servicio es quien usara al modelo para insertar un proyecto
	Proyecto saved = proyectoService.save(data.getDescripcion());
    //Un ReponseEntiy es una clase hija de HttpEntity 
    //que agrega un HttpStatus (codigo http). 
    //Esta clase se usa en RestTemplate y en los metodos de los @Controller. 
    if (saved == null) {
      return ResponseEntity.status(500).build(); //Fallo, manda el error
    }
    return ResponseEntity.ok(saved); //Un bonito 200
  }
	@DeleteMapping("borrar/{bor}")
	@ResponseBody
	public ResponseEntity<String> borrar(@PathVariable int bor){
		proyectoService.borrar(bor);
		return ResponseEntity.ok("borrado");
}	
	@PostMapping("editar/{inse}")
	@ResponseBody
	public ResponseEntity<String>editar(@PathVariable String inse){
		proyectoService.editar(inse);
		return ResponseEntity.ok("editado");
	}
	@PutMapping("actualizar/{cve}/{actu}")
	@ResponseBody
	public ResponseEntity<String>actualizar(@PathVariable int cve , @PathVariable String actu,@PathVariable Date d){
		proyectoService.actualizar(cve,actu,d);
		return ResponseEntity.ok("Actualizado");
	}
  @DeleteMapping("") //Cuando llamen a proyectos mediante un DELETE haces esto
  @ResponseBody
  public ResponseEntity<String> borraProyecto() {
    //El servicio es quien usara al modelo para insertar un proyecto
	proyectoService.borra();
	return ResponseEntity.ok("Borrado"); //Un bonito 200
  }
  
  @DeleteMapping("/borraProy/{id}")
  @ResponseBody
  public ResponseEntity<String> borraProy(@PathVariable int id){
	  proyectoService.borraUnProyecto(id);
	  return ResponseEntity.ok("Proyecto Borrado ");
  }
  
  @PostMapping("/guardar/{descripcion}")
  @ResponseBody
  public ResponseEntity<String> guardar(@PathVariable String descripcion){
	  proyectoService.save(descripcion);
	  return ResponseEntity.ok("Proyecto Agregado");
  }
  
  @PutMapping("modificar/{clave}/{descripcion}")
  @ResponseBody
  public ResponseEntity<String> modificar(@PathVariable int clave,@PathVariable String descripcion){
	  if(descripcion == null) {
		  return ResponseEntity.status(500).build();
	  }else {
		  proyectoService.modificar(clave, descripcion);
		  return ResponseEntity.ok("Has modificado al valor: "+descripcion);
	  }
  }
  
  @PutMapping("modificarFecha/{clave}/{fecha}")
  public ResponseEntity<String> modificarFecha(@PathVariable int clave, @PathVariable Date fecha){
	  proyectoService.modificandoFecha(clave,fecha);
	  return ResponseEntity.ok("La Fecha ha sido modificada");
  } 
}


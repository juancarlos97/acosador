package mx.uaemex.fi.mex_ico.acosador;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * POJO para instanciar Proyectos de y hacia la base de datos
 * @author fchavez Francisco Ch&aacute;vez Casta&ntilde;eda
 */
@Entity //Es una entidad
@Table(name = "proyectos") //Que tabla usa
public class Proyecto {

  /**
   * Clave del proyecto, marcado como llave primaria (Id)
   * que ser&aacute; autogenerada por la base de datos (GeneratedValue).
   * Asociado con la columna clave de la tabla.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)//Auto generado por la base
  @Column(name = "clave", nullable = false)
  private Integer clave;
  /**
   * Nombre del proyecto, asociado con la columna descripcion
   * de la tabla 
   */
  @Column(name = "descripcion", nullable = false, length = 128)
  private String descripcion;


/**
   * Fecha de creaci&oacute;n del proyecto en la base, asociado
   * a la columna fecha_creacion. 
   */
  @Column(name = "fecha_creacion", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date fecha_creacion;

  /**
   * Constructora sin par&aacute;metros, requisito para tener 
   * un POJO.
   */
  public Proyecto() {
  }
  /**
   * Constructora
   * @param text Nombre del proyecto
   */
  public Proyecto(String text) {
    this.descripcion = text;
    this.fecha_creacion = new Date();
  }

  public Integer getId() {
    return this.clave;
  }

  public String getText() {
    return this.descripcion;
  }

  public Date getCreatedDate() {
    return this.fecha_creacion;
  }
  
  public String getDescripcion() {
	return descripcion;
  }
  public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
  }
  
	public Date getFecha_creacion() {
		return fecha_creacion;
	}
	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
  //Los m&eacute;todos equals y hashCode son necesarios
  //para hibernate, gracias a ellos puede distinguir entre
  //objetos
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Proyecto proyecto = (Proyecto) o;
    return Objects.equals(clave, proyecto.clave);
  }

  @Override
  public int hashCode() {
    return Objects.hash(clave);
  }
}

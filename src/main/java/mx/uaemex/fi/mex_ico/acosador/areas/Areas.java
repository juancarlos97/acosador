package mx.uaemex.fi.mex_ico.acosador.areas;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="areas")

public class Areas {
	
	
	
	public Areas() {
	}
	
	public Areas(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCve() {
		return cve;
	}

	public void setCve(Integer cve) {
		this.cve = cve;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cve", nullable = false)
	private Integer cve;
	
	@Column(name = "nombre", nullable = false, length = 30)
	private String nombre;
	
	@Override
	public boolean equals(Object o) {
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;
		Areas areas = (Areas) o;
		return Objects.equals(cve , areas.cve);
	}
	
	@Override
	  public int hashCode() {
	    return Objects.hash(cve);
	  }
}

package mx.uaemex.fi.mex_ico.acosador.proyectoFinanciamiento;

//import java.sql.Date;
import java.util.Objects;
import java.util.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * POJO para instanciar Proyectos de y hacia la base de datos
 * @author fchavez Francisco Ch&aacute;vez Casta&ntilde;eda
 */
@Entity //Es una entidad
@Table(name = "proyecto_financiamiento") //Que tabla usa
public class ProyectoFinaciamiento {

  /**
   * Clave del proyecto, marcado como llave primaria (Id)
   * que ser&aacute; autogenerada por la base de datos (GeneratedValue).
   * Asociado con la columna clave de la tabla.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)//Auto generado por la base
  @Column(name = "cve", nullable = false)
  private Integer cve;

  @Column(name = "nombre", nullable = false, length = 128)
  private String nombre;

  @Column (name = "cve_proy", nullable = false, length = 128)
  private String cve_proy;
  
  @Column(name = "cve_fondo", nullable = false, length = 128)
  private String cve_fondo;
  
  @Column(name = "fech_ini", nullable = false, length = 128)
  @Temporal(TemporalType.TIMESTAMP)
  private Date fech_ini;
  
  @Column(name = "fech_fin", nullable = false, length = 128)
  @Temporal(TemporalType.TIMESTAMP)
  private Date fech_fin;
  
  @Column(name = "cve_area", nullable = false, length = 128)
  private int cve_area;
  
  @Column(name = "cve_arc", nullable = false, length = 128)
  private int cve_arc;
  
  /**
   * Constructora sin par&aacute;metros, requisito para tener 
   * un POJO.
   */

  public ProyectoFinaciamiento (){
}
  /**
   * Constructora
   * @param text Nombre del proyecto
   */
  public ProyectoFinaciamiento(String nom, String cvProy, String cveFondo ,Date fech_ini, Date fech_fin,int cveA,int cveArc) {
    this.nombre = nom;
    this.cve_proy= cvProy;
    this.cve_fondo= cveFondo;
    this.fech_ini = fech_ini;
    this.fech_fin = fech_fin;
    this.cve_area = cveA;
    this.cve_arc= cveArc;
  }
  
	public Integer getCve() {
		return this.cve;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCve_proy() {
		return cve_proy;
	}
	public void setCve_proy(String cve_proy) {
		this.cve_proy = cve_proy;
	}
	public String getCve_fondo() {
		return cve_fondo;
	}
	public void setCve_fondo(String cve_fondo) {
		this.cve_fondo = cve_fondo;
	}
	public Date getFech_ini() {
		return fech_ini;
	}
	public void setFech_ini(Date fech_ini) {
		this.fech_ini = fech_ini;
	}
	public Date getFech_fin() {
		return fech_fin;
	}
	public void setFech_fin(Date fech_fin) {
		this.fech_fin = fech_fin;
	}
	public int getCve_area() {
		return cve_area;
	}
	public void setCve_area(int cve_area) {
		this.cve_area = cve_area;
	}
	public int getCve_arc() {
		return cve_arc;
	}
	public void setCve_arc(int cve_arc) {
		this.cve_arc = cve_arc;
	}
//Los m&eacute;todos equals y hashCode son necesarios
  //para hibernate, gracias a ellos puede distinguir entre
  //objetos
	
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProyectoFinaciamiento proyectoFin = (ProyectoFinaciamiento) o;
    return Objects.equals(cve, proyectoFin.cve);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cve);
  }
}
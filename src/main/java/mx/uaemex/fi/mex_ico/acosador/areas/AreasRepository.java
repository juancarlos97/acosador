package mx.uaemex.fi.mex_ico.acosador.areas;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import mx.uaemex.fi.mex_ico.acosador.Proyecto;

@Component
public class AreasRepository {
	
	 private SessionFactory sessionFactory;
	 
	 public AreasRepository(SessionFactory sessionFactory) {
		 this.sessionFactory=sessionFactory;
	 }
	 
	 public Areas guardarArea(Areas area) {
		 Session session = sessionFactory.getCurrentSession();
		 session.save(area);
		 return area;
	 }
	 
	 public void borraArea(Areas area){
		 Session session = sessionFactory.getCurrentSession();
		 session.delete(area);
	 }
	 
	 public List<Areas> consultaArea(int cve){
		 List<Areas> area;
		 Areas a = new Areas();
		 Session session = sessionFactory.getCurrentSession();
		 Query query=session.createQuery("from "+ a.getClass().getName()+" where cve= "+cve);
		 area = query.list();
		 return area;	  
	}
	 
	 public void modificarArea(Areas area) {
			Session session = sessionFactory.getCurrentSession();
			session.update(null, area);	  
	  }

}

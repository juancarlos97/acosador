package mx.uaemex.fi.mex_ico.acosador;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import java.util.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ProyectoService {
  private static final Logger log = LoggerFactory.getLogger(ProyectoService.class);
  private ProyectoRepository repository;

  public ProyectoService(ProyectoRepository repository) {
    this.repository = repository;
  }

  @SecurityCheck
  @Transactional(noRollbackFor = { UnsupportedOperationException.class })
  public Proyecto save(String text) {
	Proyecto proyecto = repository.saveProyecto(new Proyecto(text));   
	log.debug("Nuevo proyecto[id={}] salvado", proyecto.getId());
    //updateStatistics(); mensaje innecesario
    return proyecto;
  }
  
  @SecurityCheck
  @Transactional()
  public void borra() {
	ArrayList<Proyecto> proys = (ArrayList<Proyecto>) this.consultaProyectos();
	Proyecto proy = proys.get(0);
	repository.deleteProyecto(proy);
	log.debug("Borrado proyecto[id={}] adios", 1);
  }
  
  @SecurityCheck
  @Transactional()
  public void modificar(int id, String Descripcion) {
	  ArrayList<Proyecto> proy=(ArrayList<Proyecto>) this.consultaProyecto(id);
	  Proyecto proyecto = proy.get(0);
	  proyecto.setDescripcion(Descripcion);
	  repository.modificarProyecto(proyecto,Descripcion);
	  log.debug("Modificado, a ver, consulta la base ");
	  
  }
  
  @SecurityCheck
  @Transactional()
  public void borraUnProyecto(int id) {
	  ArrayList<Proyecto> proy = (ArrayList<Proyecto>) this.consultaProyecto(id);
	  Proyecto proye = proy.get(0);
	  repository.deleteProyecto(proye);
	  log.debug("Borrado proyecto[id={}] adios", 1);
  }
  
  public List<Proyecto> consultaProyecto(int id){
	  return repository.consultaProyecto(id);
  }
  
  public List<Proyecto> consultaProyectos(){
	  return repository.consultaProyectos();
  }
  
  @SecurityCheck
  @Transactional()
  public void modificandoFecha(int clave, java.util.Date fecha){
	ArrayList<Proyecto> proye = (ArrayList<Proyecto>) this.consultaProyecto(clave);
	Proyecto pro=proye.get(0);
	pro.setFecha_creacion(fecha);
	repository.modificaFecha(pro);
	log.debug("Fecha Modificada",1);
	
  } 
  
  private void updateStatistics() {
	throw new UnsupportedOperationException("This method is not implemented yet");
  }
 
  
  public List<Proyecto> verProy(long cve){
	  return repository.consultaProyecto(cve);
  }
  @SecurityCheck
  @Transactional()
  public void borrar(int bor) {
	  ArrayList<Proyecto> proy= (ArrayList<Proyecto>) this.verProy(bor);
	  Proyecto reg=proy.get(0);
	  repository.deleteProyecto(reg);
  }
  @SecurityCheck
  @Transactional()
  public void editar(String text) {
	  Proyecto insert = new Proyecto(text);
	  repository.saveProyecto(insert);
  }
  @SecurityCheck
  @Transactional()
  public void actualizar(int cve, String actu, java.util.Date d) {
	  ArrayList <Proyecto> clva=(ArrayList<Proyecto>) this.verProy(cve);
	  Proyecto indiact=clva.get(0);
	  indiact.setDescripcion(actu);
	  indiact.setFecha_creacion(d);
	  repository.saveProyecto(indiact);
  }
}
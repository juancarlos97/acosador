package mx.uaemex.fi.mex_ico.acosador;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import mx.uaemex.fi.mex_ico.acosador.upload.storage.StorageProperties;
import mx.uaemex.fi.mex_ico.acosador.upload.storage.StorageService;


@SpringBootApplication //Levanta el Spring-Boot y alli ejecuta la aplicacion
@EnableConfigurationProperties(StorageProperties.class)
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
  
  @Bean
  CommandLineRunner init(StorageService storageService) {
      return (args) -> {
          storageService.deleteAll();
          storageService.init();
      };
  }
}
package mx.uaemex.fi.mex_ico.acosador.proyectoFinanciamiento;

import org.slf4j.Logger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import mx.uaemex.fi.mex_ico.acosador.SecurityCheck;

@Component
public class ProyFinanService {
  private static final Logger log = LoggerFactory.getLogger(ProyectoFinaciamiento.class);
  private ProyFinanRepository repository;

  public ProyFinanService(ProyFinanRepository repository) {
    this.repository = repository;
  }

  @SecurityCheck
  @Transactional(noRollbackFor = { UnsupportedOperationException.class })
  public ProyectoFinaciamiento save(String nom, String cvProy, String cveFondo ,Date fech_ini, Date fech_fin,int cveA,int cveArc) {
	  ProyectoFinaciamiento proyecto = repository.saveProyecto(new ProyectoFinaciamiento(nom, cvProy, cveFondo ,fech_ini, fech_fin, cveA, cveArc));   
	log.debug("Nuevo proyecto[id={}] salvado", proyecto.getCve());
    updateStatistics();
    return proyecto;
  }
  
  private void updateStatistics() {
	throw new UnsupportedOperationException("This method is not implemented yet");
  }
   
  public List<ProyectoFinaciamiento> verProy(long cve){
	  return repository.consultaProyecto(cve);
  }
  @SecurityCheck
  @Transactional()
  public void borrar(int bor) {
	  ArrayList<ProyectoFinaciamiento> proy= (ArrayList<ProyectoFinaciamiento>) this.verProy(bor);
	  ProyectoFinaciamiento reg=proy.get(0);
	  repository.deleteProyecto(reg);
  }
  @SecurityCheck
  @Transactional()
  public void editar(String nom, String cvProy, String cveFondo ,Date fech_ini, Date fech_fin,int cveA,int cveArc) {
	  ProyectoFinaciamiento insert = new ProyectoFinaciamiento(nom, cvProy, cveFondo ,fech_ini, fech_fin, cveA, cveArc);
	  repository.insertProyecto(insert);
  }
  @SecurityCheck
  @Transactional()
  public void actualizar(int cve ,String nom,String cvProy,String cvFondo,Date fech_in,Date fech_fi,int cvA,int cvAr) {
	  ArrayList <ProyectoFinaciamiento> cl=(ArrayList<ProyectoFinaciamiento>) this.verProy(cve);
	  ProyectoFinaciamiento indiact=cl.get(0);
	  indiact.setNombre(nom);
	  indiact.setCve_proy(cvProy);
	  indiact.setCve_fondo(cvFondo);
	  indiact.setFech_fin(fech_in);
	  indiact.setFech_fin(fech_fi);
	  indiact.setCve_area(cvA);
	  indiact.setCve_arc(cvAr);
	  repository.actualizarProyecto(indiact);
  }
  
  public List<ProyectoFinaciamiento> consultar(){
	  return repository.consultaProyectos();
  }

}